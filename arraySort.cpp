#include <iostream>
#include <string>
#include <fstream>
#include <iomanip>

using namespace std;

void printArrays(const string firstNames[], const string lastNames[], const double heights[], int size, ostream &outputStream);
void sort(string array[], int newIndices[], int size);
void sort(double array[], int newIndices[], int size);
void resetIndices(int indices[], int size);
void copyArray(string originalArray[], string newArray[], int size);
void copyArray(double originalArray[], double newArray[], int size);
void swap(double &a, double &b);
void swap(string &a, string &b);
void fixArrays(string arr1[], double arr2[], int indices[], int size);
void fixArrays(string arr1[], string arr2[], int indices[], int size);

int main() {
	/// Create arrays
	// Declare arrays
	const int NUM_CLASSMATES = 16;
	// Data
	string firstNames[NUM_CLASSMATES] = {"Amir", "Cat", "Daniel", "Hemal", "Jericho", "Josue", "Karlos", "Nassima", "Nhu", "Nicole", "Qiming", "Rigoberto", "Samuel", "Shibin", "Shing", "Zelalem"};
	string lastNames[NUM_CLASSMATES] = {"Tal", "Nguyen", "Richardson", "Patel", "Keyne", "Sanchez", "Boehlke", "Yahiaoui", "Nguyen", "Foti", "Chen", "Rosa", "Silverman", "Cai", "Lau", "Yitbarek"};
	//double heights[NUM_CLASSMATES] = {65 / (double)12, 62 / (double)12, 69 / (double)12, 67 / (double)12, 70 / (double)12, 71 / (double)12, 63 / (double)12, 72 / (double)12, 60 / (double)12, 70 / (double)12, 66 / (double)12, 64 / (double)12, 68 / (double)12, 69 / (double)12, 70 / (double)12, 72 / (double)12, 67 / (double)12};
	double heights[NUM_CLASSMATES] = {6.0833333333333, 5.3, 5.75, 5.8, 5.916666666667, 5.8, 5.916666666667, 5.916666666667, 5.416666666667, 5.25, 6, 5.583333333333, 6.25, 5, 5.9, 5.9};
	int newIndices[NUM_CLASSMATES];
	resetIndices(newIndices, NUM_CLASSMATES);
	string tempFirstNames[NUM_CLASSMATES];
	string tempLastNames[NUM_CLASSMATES];
	double tempHeights[NUM_CLASSMATES]; 

	/// Create output file stream
	ofstream outputFile;
	outputFile.open("output.txt");
	if (outputFile.fail()) {
		cout << "Failed to open output file" << endl;
		return 1;
	}

	/// Sort arrays and print
	// Sort by first names
	cout << "Sorting by first names" << endl;
	outputFile << "Sorting by first names" << endl;
	sort(firstNames, newIndices, NUM_CLASSMATES);

	fixArrays(lastNames, heights, newIndices, NUM_CLASSMATES);

	printArrays(firstNames, lastNames, heights, NUM_CLASSMATES, cout);
	printArrays(firstNames, lastNames, heights, NUM_CLASSMATES, outputFile);
	
	// Sort by last names
	cout << endl << "Sorting by last names" << endl;
	outputFile << endl << "Sorting by last names" << endl;
	sort(lastNames, newIndices, NUM_CLASSMATES);

	fixArrays(firstNames, heights, newIndices, NUM_CLASSMATES);

	printArrays(firstNames, lastNames, heights, NUM_CLASSMATES, cout);
	printArrays(firstNames, lastNames, heights, NUM_CLASSMATES, outputFile);

	// Sort by height
	cout << endl << "Sorting by height" << endl;
	outputFile << endl << "Sorting by height" << endl;
	sort(heights, newIndices, NUM_CLASSMATES);

	fixArrays(firstNames, lastNames, newIndices, NUM_CLASSMATES);

	printArrays(firstNames, lastNames, heights, NUM_CLASSMATES, cout);
	printArrays(firstNames, lastNames, heights, NUM_CLASSMATES, outputFile);
	return 0;
}

// Prints the each value of the three arrays to outputStream
void printArrays(const string firstNames[], const string lastNames[], const double heights[], int size, ostream &outputStream) {
	outputStream << fixed << setprecision(2);
	for (int i = 0; i < size; i++) {
		outputStream << firstNames[i] << ":" << lastNames[i] << ":" << heights[i] << " feet" << endl;
	}
}

void copyArray(string originalArray[], string newArray[], int size) {
	for (int i = 0; i < size; i++) {
		newArray[i] = originalArray[i];
	}
}

void copyArray(double originalArray[], double newArray[], int size) {
	for (int i = 0; i < size; i++) {
		newArray[i] = originalArray[i];
	}
}

void fixArrays(string arr1[], double arr2[], int indices[], int size) { 
	string temp1[size];
	double temp2[size];
	copyArray(arr1, temp1, size);
	copyArray(arr2, temp2, size);
	for (int i = 0; i < size; i++) {
		arr1[i] = temp1[indices[i]];
		arr2[i] = temp2[indices[i]];
	}
	resetIndices(indices, size);
}

void fixArrays(string arr1[], string arr2[], int indices[], int size) {
	string temp1[size];
	string temp2[size];
	copyArray(arr1, temp1, size);
	copyArray(arr2, temp2, size);
	for (int i = 0; i < size; i++) {
		arr1[i] = temp1[indices[i]];
		arr2[i] = temp2[indices[i]];
	}
	resetIndices(indices, size);
}

// Sorts an array of strings using the bubble sort alogrithm from Pr8-4 in the textbook, and puts the new indices in newIndices
void sort(string array[], int newIndices[], int size) {
	int maxElement;
	int index;
	for (maxElement = size-1; maxElement > 0; maxElement--) {
		for (index = 0; index < maxElement; index++) {
			if (array[index].compare(array[index + 1]) > 0) {
				swap(array[index], array[index+1]);
				swap(newIndices[index], newIndices[index +1]);
			}
		}
	}
}

// Sorts an array of doubles using the bubble sort alogrithm from Pr8-4 in the textbook, and puts the new indices in newIndices
void sort(double array[], int newIndices[], int size) { // From textbook Pr8-4, modified to swap the names as well
	int maxElement;
	int index;
	for (maxElement = size - 1; maxElement > 0; maxElement--) {
		for (index = 0; index < maxElement; index++) {
			if (array[index] > array[index + 1]) {
				swap(array[index], array[index+1]);
				swap(newIndices[index], newIndices[index + 1]);
			}
		}
	}
}

// Resets the elements in the indices array
void resetIndices(int indices[], int size) {
	for (int i = 0; i < size; i++) {
		indices[i] = i;
	}
}

// Swaps two doubles
void swap(double &a, double &b) {
	double temp = a;
	a = b;
	b = temp;
}

// Swaps two strings
void swap(string &a, string &b) {
	string temp = a;
	a = b;
	b = temp;
}
